/*
*	UNTITLED GAME
*	Author: Andrew Napper
*	Started: August 13th 2015
*	When the page is ready, set the state to main and start the engine
*
*/

var gameState = {
		MAIN : 0,
		CHAR_SELECT : 1,
		IN_GAME : 2,
	};

var SPEED = 3;

var activeGameState = gameState.MAIN;
var canvas = $('#gameCanvas')[0];
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
var context = canvas.getContext('2d');
var gridWidth = 80;
var gridHeight = 40;
var gridLocationSize = 64;
var world = new World(gridWidth, gridHeight, gridLocationSize);

// controls
var mousePosition = new Point(0,0);
var direction = new Point(0,0);
var movementThreshold = 20;
var posUp = movementThreshold;
var posDown = canvas.height - movementThreshold;
var posLeft = movementThreshold;
var posRight = canvas.width - movementThreshold;

$( document ).ready( function() {
	world.drawWorld();
	function start(){
		requestAnimationFrame(start);
		render();
	};
	start();
});

function updateMouseDirection(e){
	mousePosition.x = e.clientX;
	mousePosition.y = e.clientY;

	console.log(e);
		// north
		if(mousePosition.y < posUp) 
		{
			// northeast
			if(mousePosition.x > posRight)
			{
				direction.x = SPEED;
				direction.y = -SPEED;
			}
			// northwest
			else if(mousePosition.x < posLeft)
			{
				direction.x = -SPEED;
				direction.y = -SPEED;
			}
			// north
			else
			{
				direction.x = 0;
				direction.y = -SPEED;
			}
		}

		// south
		else if(mousePosition.y > posDown)
		{
			// southwest
			if(mousePosition.x < posLeft)
			{
				direction.x = -SPEED;
				direction.y = SPEED;
			}
			// southeast
			else if(mousePosition.x > posRight)
			{
				direction.x = SPEED;
				direction.y = SPEED;
			}
			//south
			else
			{
				direction.x = 0;
				direction.y = SPEED;
			}
		}
		// east
		else if(mousePosition.x > posRight)
		{
			direction.x = SPEED;
			direction.y = 0;
		}
		// west
		else if(mousePosition.x < posLeft)
		{
			direction.x = -SPEED;
			direction.y = 0;
		}
		// none
		else
		{
			direction.x = 0;
			direction.y = 0;
		}
};

canvas.addEventListener("mousemove", updateMouseDirection);	

function render(){
	world.move(direction.x, direction.y);
	//context.drawImage(this.world.getWorldCanvas(), 0, 0);
}

// This is the world which consists of an image and a location
function World(gridWidth, gridHeight, gridLocationSize){
	this.worldLocation = new Point(0,0);
	this.worldImage = new Image();
	this.worldImage.src = "world/world1.png";
	this.worldWidth = gridWidth*gridLocationSize;
	this.worldHeight = gridHeight*gridLocationSize;
	this.worldBufferCanvas = document.createElement('canvas');
	this.worldBufferContext = this.worldBufferCanvas.getContext('2d');
	this.worldBufferCanvas.width = window.innerWidth;
	this.worldBufferCanvas.height = window.innerHeight;

	this.getWorldCanvas = function(){
		return this.worldBufferCanvas;
	};

	this.move = function(xMove, yMove){
		var newX = this.worldLocation.x + xMove;
		var newY = this.worldLocation.y + yMove;

		if(this.canMove(newX, newY))
		{
			this.worldLocation.x += xMove;
			this.worldLocation.y += yMove;
			this.drawWorld();
			//this.velocity.x = xMove;
			//this.velocity.y = yMove;
			//this.update();
		}
	};

	this.canMove = function(newX, newY){
		var canMove = true;
		var rightEdge = newX + this.worldBufferCanvas.width;
		var bottomEdge = newY + this.worldBufferCanvas.height;
	
		if((rightEdge > this.worldWidth) || (newX < 0))
		{
			canMove = false;
		}

		if((bottomEdge > this.worldHeight) || (newY < 0))
		{
			canMove = false;
		}

		return canMove;
	};

	this.drawWorld = function(){
		context.drawImage(this.worldImage, this.worldLocation.x, this.worldLocation.y, this.worldBufferCanvas.width, this.worldBufferCanvas.height, 0, 0, this.worldBufferCanvas.width, this.worldBufferCanvas.height);
	}

}

// Set up a point object
function Point(x, y){
	this.x = x;
	this.y = y;
} 